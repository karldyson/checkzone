#!/usr/bin/perl

$|++;

use strict;

use Bagless::Utils qw/resolve interactive/;

my $debug = $ENV{BAGLESS_DEBUG} || 0;

my @zones = @ARGV;

my @named_config_files = qw|
	/etc/bind/named.conf.master
|;

unless(@zones) {
	open(ZONES,'/usr/local/etc/domains') || die "cannot open zones file\n";
	while(<ZONES>) {
		chomp;
		s/^[\s]+#//;
		next if m/^#/;
		print "Found domain $_\n" if $debug;
		push @zones, $_;
	}
	close ZONES;
}

print "Domains list:\n".join("\n", @zones)."\n" if $debug;

my @fail;

my $local_zones;
for(@named_config_files) {
	my $f = $_;
	print "Hunting for local zones in $f\n" if $debug;
	if(open(M, $f)) {
		while(<M>) {
			chomp;
			if(m/zone "(.*?)" /) {
				$local_zones->{$1} = 1;
				print "  found $1\n" if $debug;
			} else {
				next;
			}
		}
		close M;
	}
}

for(@zones) {
	my $z = $_;
	print "debug: checking $z\n" if $debug;
	my $output = "checking zone $z...\n";
	my $ns_master;
	if($local_zones->{$z} > 0) {
		$output .= "\tZone is locally mastered\n";
		print "\tZone is locally mastered\n" if $debug;
		$ns_master = '127.0.0.1';
	} else {
		$ENV{RECURSE} = 1;
		my $soa_master = resolve($z, 'SOA');
		unless(defined $soa_master) {
			push @fail, "FAILED to obtain SOA for $z: $Bagless::Utils::errstr";
			next;
		}
		$ns_master = $soa_master->mname;
	}
	$output .= "\tGot master NS $ns_master\n";
	print "Got master NS $ns_master\n" if $debug;
	$ENV{RECURSE} = 0;
	my @ns = resolve($z, 'NS', $ns_master);
	my $soa_from_master = resolve($z, 'SOA', $ns_master);
	$output .= "\tGot serial ".$soa_from_master->serial." from master NS $ns_master\n";
	print "\tGot serial ".$soa_from_master->serial." from master NS $ns_master\n" if $debug;
	if(@ns > 0) {
		my $master_serial = $soa_from_master->serial;
		my $fail = 0;
		for(@ns) {
			my $ns = $_;
			next unless $ns;
			$ENV{RES_NAMESERVERS} = $ns;
			print "\tChecking NS $ns\n" if $debug;
			my $soa = resolve($z,'SOA',$ns);
			unless($soa) {
				print "\tUnable to contact NS $ns for zone $z: $Bagless::Utils::errstr\n" if $debug;
				push @fail, "FAILED to contact NS $ns for zone $z: $Bagless::Utils::errstr";
				next;
			}
			my $serial = $soa->serial;
			print "\tNS: $ns: $serial\n" if $debug;
			$output .= "\tNS: $ns: $serial";
			if($serial != $master_serial) {
				$output .= " .. FAIL\n";
				$fail++;
			} else {
				$output .= " .. OK\n";
			}
		}
		push @fail, $output if $fail;
	}
	else {
		print "$z\tNS: FAIL: $Bagless::Utils::errstr\n";
	}
	print $output if interactive();
	#print $output;
}

if(@fail) {
	print "\nThe following zones had errors:\n\n";
	for(@fail) {
		print "$_\n";
	}
}
