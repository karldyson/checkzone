#!/usr/bin/perl

# http://stackoverflow.com/questions/6221868/why-does-this-print-12-times#6222082

$|++;

use strict;

use threads;
use threads::shared;

use Thread::Queue;

use Bagless::Utils qw/resolve interactive/;

use Switch;
use Date::Parse;
use Date::Calc qw/Delta_Days Today/;
use Time::HiRes qw/time/;

my $debug = $ENV{BAGLESS_DEBUG} || 0;

my $expiry_cutoff = 30;
use POSIX qw/strftime/;
my $now_hour = strftime "%H", localtime;
my $warnexpiry = $now_hour == 6 ? 1 : 0;
$warnexpiry++ if interactive();
print "Now Hour is $now_hour\n" if $debug;
print "Warn about expiry is $warnexpiry\n" if $debug;

#my @zones = @ARGV;

my $q = new Thread::Queue;

my @named_config_files = qw|
	/etc/bind/named.conf.master
|;

my $local_zones;
for(@named_config_files) {
	my $f = $_;
	print "Hunting for local zones in $f\n" if $debug;
	if(open(M, $f)) {
		while(<M>) {
			chomp;
			if(m/zone "(.*?)" /) {
				$local_zones->{$1} = 1;
				print "  found $1\n" if $debug;
			} else {
				next;
			}
		}
		close M;
	}
}

my @extra_ns;
if(open(E, "/usr/local/etc/checkzone_extra_ns")) {
	while(<E>) {
		next if m/^\s*#/;
		chomp;
		push @extra_ns, $_;
	}
	close E;
}

my $workers = 5;
my @worker_threads;
my @fail:shared;
my %rate:shared;
for(1..$workers) {
	push @worker_threads, async {
		while(defined(my $zone = $q->dequeue())) {
			my $return = checkzone($zone);
			my $output = $return->{output};
			for(@{$return->{fail}}) {
				push @fail, $_;
			}
			print $output if interactive();
		}
	}
}

if(@ARGV) {
	$q->enqueue(@ARGV);
} else {
	open(ZONES,'/usr/local/etc/domains') || die "cannot open zones file\n";
	while(<ZONES>) {
		chomp;
		s/^[\s]+#//;
		next if m/^#/;
		print "Found domain $_\n" if $debug;
		#push @zones, $_;
		$q->enqueue($_);
	}
	close ZONES;
}

$q->enqueue(undef) for @worker_threads;

$_->join for @worker_threads;

if(@fail) {
	print "\nThe following errors occured:\n\n";
	for(@fail) {
		print "$_\n";
	}
}

sub checkzone {
	my $z = shift;
	print "debug: checking $z\n" if $debug;
	my $output = "checking zone $z...\n";
	my $extra_ns;
	if(@extra_ns) {
		$extra_ns = $extra_ns[rand(@extra_ns)];
		$output .= "\tSelected extra NS $extra_ns for this check\n";
	}
	my @fail;
	my $ns_master;
	if($local_zones->{$z} > 0) {
		$output .= "\tZone is locally mastered\n";
		print "\tZone is locally mastered\n" if $debug;
		$ns_master = '127.0.0.1';
	} else {
		$ENV{RECURSE} = 1;
		my $soa_master = resolve($z, 'SOA');
		unless(defined $soa_master) {
			push @fail, "FAILED to obtain SOA for $z: $Bagless::Utils::errstr";
			next;
		}
		$ns_master = $soa_master->mname;
	}
	$output .= "\tGot master NS $ns_master\n";
	print "Got master NS $ns_master\n" if $debug;
	$ENV{RECURSE} = 0;
	my @ns = resolve($z, 'NS', $ns_master);
	push @ns, $extra_ns if $extra_ns;
	my $soa_from_master = resolve($z, 'SOA', $ns_master);
	unless($soa_from_master) {
		print "\tUnable to contact Master NS $ns_master for zone $z: $Bagless::Utils::errstr\n" if $debug;
		$output .= "\tFAILED to contact Master NS $ns_master: $Bagless::Utils::errstr\n";
		push @fail, "FAILED to contact Master NS $ns_master for zone $z: $Bagless::Utils::errstr";
		next;
	}
	$output .= "\tGot serial ".$soa_from_master->serial." from master NS $ns_master\n";
	print "\tGot serial ".$soa_from_master->serial." from master NS $ns_master\n" if $debug;
	if(@ns > 0) {
		my $master_serial = $soa_from_master->serial;
		my $fail = 0;
		for(@ns) {
			my $ns = $_;
			next unless $ns;
			$ENV{RES_NAMESERVERS} = $ns;
			print "\tChecking NS $ns\n" if $debug;
			$ENV{RECURSE} = 1 if $ns eq $extra_ns;
			my $soa = resolve($z,'SOA',$ns);
			$ENV{RECURSE} = 0 if $ns eq $extra_ns;
			unless($soa) {
				print "\tUnable to contact NS $ns for zone $z: $Bagless::Utils::errstr\n" if $debug;
				$output .= "\tFAILED to contact NS $ns: $Bagless::Utils::errstr\n";
				push @fail, "FAILED to contact NS $ns for zone $z: $Bagless::Utils::errstr";
				next;
			}
			my $serial = $soa->serial;
			print "\tNS: $ns: $serial\n" if $debug;
			$output .= "\tNS: $ns: $serial";
			if($serial != $master_serial) {
				$output .= " .. FAIL\n";
				$fail++;
			} else {
				$output .= " .. OK\n";
			}
		}
		push @fail, $output if $fail;
	}
	else {
		print "$z\tNS: FAIL: $Bagless::Utils::errstr\n";
	}

	my $expiry = check_expiry($z);
	unless($expiry->{status} eq 'SKIPPED') {
		my $days = $expiry->{days} < 0 ? $expiry->{days} * -1 : $expiry->{days};
		my $tense = $days < 0 ? "expired $days days ago" : "expires in $days days";
		if($expiry->{status} =~ m/(WARN|FATAL)/) {
			push @fail, "\t$z expiry $expiry->{status}: $tense\n" if $warnexpiry;
		}
		$output .= "\t$z expiry $expiry->{status}: $tense\n";
	}

	my $return;
	$return->{output} = $output;
	@{$return->{fail}} = @fail;
	return $return;
}

sub check_expiry {
	my $ret;
	return {'status'=>'SKIPPED', 'days'=>9999} unless $warnexpiry;
	my $z = shift;
	my($tld) = $z =~ m/\.(\w+|(?:(?:uk|gb)\.(?:net|com)))$/;
	my $whois_svr;
	switch($tld) {
		case /je/ { return {'status'=>'OK', 'days'=>9999} }
		case /gg/ { return {'status'=>'OK', 'days'=>9999} }
		else { $whois_svr = "$tld.whois-servers.net" }
	}
	print "Expiry: Using server $whois_svr\n" if $debug;
	my $rate_time = time() - $rate{$tld};
	if($rate_time < 1) {
		my $wait = 1;
		print "Rate limiting $tld whois\n" if $debug;
		sleep $wait;
	}
	open(W, "whois -h $whois_svr $z|") || die "cannot whois: $!\n";
	$rate{$tld} = time();
	my @whois = <W>;
	close W;
	my $expiry;
	foreach(@whois) {
		chomp;
		if(m/expir.*?:(.*)$/i) {
			$expiry = $1;
			print "Expiry: Located date is [$expiry]\n" if $debug;
			$expiry =~ s/^\s*//;
			$expiry =~ s/\s*$//;
			$expiry =~ s/\s\d{2}:\d{2}:\d{2}$//;
			#$expiry =~ s/[\/\\-]/ /g;
			($expiry) = $expiry =~ m/(\d{2}).(\d{2}).(\d{4})/ ? "$3-$2-$1" : $expiry;
			print "Expiry: Fudged date is [$expiry]\n" if $debug;
		}
	}
	if($expiry) {
		my(undef, undef, undef, $dd, $mm, $yy, undef) = strptime($expiry);
		my @expiry = ($yy+1900, ++$mm, $dd);
		print "Expiry: Parsed Date is [@expiry]\n" if $debug;
		my $days = Delta_Days(Today(),@expiry) - 1;
		print "Expiry: $days until expiry\n" if $debug;
		$ret->{days} = $days;
		$ret->{date} = sprintf "%4d-%2d-%2d", @expiry;
		if($days >= $expiry_cutoff) { $ret->{status} = 'OK'; }
		elsif($days < $expiry_cutoff) { $ret->{status} = 'WARN'; }
		elsif($days < 1) { $ret->{status} = 'FATAL'; }
		else { die "how'd we get here?\n"; }
	}
	return $ret;
}
